
const {MongoClient, ObjectID} = require('mongodb')

const url = 'mongodb://localhost:27017/job'

const test1 = async () => {
	let conn = await MongoClient.connect(url)
	let db = conn.db()
	let test = db.collection('test')

	// let obj = await test.insertOne({
	// 	name: 'lzt',
	// 	json: {
	// 		file: [{
	// 			name: 'xh',
	// 			type: 'string'
	// 		},{
	// 			name: 'wfxw',
	// 			type: 'string'
	// 		}],
	// 		url: '192.1686.1.108',
	// 		time: new Date()
	// 	}
	// })
	// console.log(obj)
	// console.log(obj.ops[0]._id)

  await test.replaceOne({
  	_id: new ObjectID('5ad7ec7bb56def1a235d642c')
  }, {
  	$set: {
  		age: 200
  	}
  }).catch(e => {
  	console.log(e)
  })

	let list = await test.find({_id: new ObjectID('5ad7ec7bb56def1a235d642c')}).toArray()

	list.forEach(x => console.log(JSON.stringify(x)))
}

const test2 = async () => {
  let conn = await MongoClient.connect(url)
  let db = conn.db()
  let logs = db.collection('logs')
  let result = await logs.find({
    begin_time: {
      $lt: new Date('2018-04-19')
    }
  }).toArray()
  result.forEach(x => console.log(x))
}

test2()
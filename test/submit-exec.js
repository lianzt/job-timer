
const rp = require('request-promise-native');

let option = {
  uri: 'http://lzt-mint:6066/v1/submissions/create',
  method: 'post',
  headers: {
    'Content-Type': 'application/json'
  },
  body: {
    action : "CreateSubmissionRequest",
    appArgs : [], 
    appResource : "hdfs://FatServer1:9000/jars/sjtp-test.jar", 
    "clientSparkVersion" : "2.1.0",
    "environmentVariables" : {
      "SPARK_ENV_LOADED" : "1"
    },
    "mainClass" : "com.lianzt.streaming.Ping",
    "sparkProperties" : {
      "spark.jars" : "hdfs://FatServer1:9000/jars/org.apache.kafka_kafka-clients-0.10.0.1.jar,hdfs://FatServer1:9000/jars/org.apache.spark_spark-sql-kafka-0-10_2.10-2.2.1.jar,hdfs://FatServer1:9000/jars/lz4-1.3.0.jar,hdfs://FatServer1:9000/jars/slf4j-api-1.7.16.jar,hdfs://FatServer1:9000/jars/snappy-java-1.1.2.6.jar,hdfs://FatServer1:9000/jars/spark-tags_2.10-2.2.1.jar,hdfs://FatServer1:9000/jars/unused-1.0.0.jar",
      "spark.driver.supervise" : "false",
      "spark.app.name" : "MyJob",
      "spark.eventLog.enabled": "true",
      "spark.submit.deployMode" : "client",
      "spark.master" : "spark://lzt-mint:6066"
    }
  },
  timeout: 60000,
  json: true // Automatically stringifies the body to JSON
}

rp(option).then(data => {
  console.log(data)
}).catch(e => {
  console.log('error:',e)
})
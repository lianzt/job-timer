const util = require('../util')
const api = require('../api')

const logger = util.getLogger('test.api-test')

// util.callLivy(api.getBatches).then((res) => {
// 	logger.debug(res)
// })

// util.callLivy(api.postBatch, {
// 	file: 'hdfs://FatServer1:9000/jtgzfw/log-file.jar',
// 	className: 'com.lianzt.Main'
// }).then(res => logger.debug(res))

util.callLivy(api.getBatchState, null, 0)
	.then(res => logger.debug(res))
	.catch(e => logger.error(e))
const { getStatus } = require('../service')
const timer = require('../timer')

timer.addTask({
  name: 'test',
  cycle: 20000,
  fun () {
    console.log(getStatus())
  }
})

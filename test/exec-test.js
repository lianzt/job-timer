const util = require('../util')

const run = async () => {
  let out = await util.runExec('hadoop fs -ls /')
  console.log(out.stdout)
  out = await util.runExec('hadoop fs -copyFromLocal /home/lzt/tmp/table.sql /test')
  console.log(out.stdout)
}

const runFile = async () => {
  try{
    let shell = '/home/lzt/centos-home/root/workspace/shell/test/testnode.sh'
    let out = await util.runExec(`chmod +x ${shell}`)
    console.log(out)
    out = await util.runShellFile(shell, undefined, {timeout: 10000})
    console.log(out)
  }catch(e){
    console.log('error:', e)
  }
}

// run()
runFile()

#! /bin/bash

npm run build

rm -f ../public/index.html
rm -rf ../public/static

cp ./dist/index.html ../public
cp -r ./dist/static ../public

cd ../public

rm -f static/css/*.map
rm -f static/js/*.map


tar cvf public.tar index.html static

cd ../admin